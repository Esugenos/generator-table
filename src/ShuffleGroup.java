import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.shuffle;

public class ShuffleGroup {
    ArrayList<String> personnes=new ArrayList<>(List.of("Cynthia","Marion","Sarah","Aurélien","PA","Stéphane","Joannie","Richard","Charles","Baptiste","Erwan","Maxime"));
    public ShuffleGroup(){
        shuffle(personnes);
    }

    public ArrayList<String> getPersonne(){

        return personnes;
    }
}
