import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class Accueil extends JWindow {

    AcceuilBg panelAcc = new AcceuilBg();
    JLabel labelTitre = new JLabel();
    JLabel labelCopy = new JLabel();
    JLabel imgDeath = new JLabel(new ImageIcon("img/death.gif"));
    JLabel labelWest = new JLabel(new ImageIcon("img/ghostWest.gif"));
    JLabel labelEast = new JLabel(new ImageIcon("img/ghostEast.gif"));

    public Accueil() throws IOException {
        this.setSize(680, 400);
        this.setLocationRelativeTo(null);

        panelAcc.setLayout(new BorderLayout());

        Font font = new Font("Comic sans ms", Font.BOLD, 25);
        Font font2 = new Font("Arial", Font.ITALIC, 10);

        labelTitre.setForeground(Color.WHITE);
        //panelAcc.setBackground(new Color(41, 79, 109));
        labelTitre.setFont(font);
        labelTitre.setText("Le jeu de la roulette russe!!");
        labelTitre.setHorizontalAlignment(JLabel.CENTER);
        labelTitre.setVerticalAlignment(JLabel.CENTER);

        labelCopy.setForeground(Color.RED);
        labelCopy.setFont(font2);
        labelCopy.setText("Copyright Jo-Albert");
        labelCopy.setHorizontalAlignment(JLabel.CENTER);
        labelCopy.setVerticalAlignment(JLabel.CENTER);

       labelEast.setHorizontalAlignment(JLabel.CENTER);
        labelEast.setVerticalAlignment(JLabel.CENTER);

        labelWest.setHorizontalAlignment(JLabel.CENTER);
        labelWest.setVerticalAlignment(JLabel.CENTER);

        panelAcc.add(imgDeath, BorderLayout.CENTER);
        panelAcc.add(labelWest, BorderLayout.WEST);
        panelAcc.add(labelEast, BorderLayout.EAST);
        panelAcc.add(labelTitre, BorderLayout.NORTH);
        panelAcc.add(labelCopy, BorderLayout.SOUTH);


        this.setContentPane(panelAcc);

        this.setVisible(true);
    }
}
