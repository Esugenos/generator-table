import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {

        Accueil accueil=new Accueil();
        Panel fenetre=new Panel();
        fenetre.setVisible(false);

        new Timer(4000, e-> {
            accueil.dispose();
            fenetre.setVisible(true);
        }).start();


    }
}
