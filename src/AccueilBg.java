import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

class AcceuilBg extends JPanel {
    private BufferedImage image;

    public AcceuilBg() throws IOException {
        image = ImageIO.read(new File("img/bg.jpg"));
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, null);
    }
}

