import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class PanelBg extends JPanel {
    private BufferedImage image;

    public PanelBg() throws IOException {
        image = ImageIO.read(new File("img/TableRonde.jpg"));
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, null);
    }
}
