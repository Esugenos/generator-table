import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Panel extends JFrame{

    PanelBg panelBg = new PanelBg();
    JPanel panel=new JPanel();
    JPanel panelButton = new JPanel();
    JButton close = new JButton("Fermer");
    JLabel labelBenoit=new JLabel("Benoît");
   // ArrayList<Integer> x=new ArrayList<>(List.of(3,73,159,352,441,541,583,522,518,327,114,1));
    //ArrayList<Integer> y=new ArrayList<>(List.of(120,106,105,109,109,118,151,244,513,554,541,516));
    List<Point>coordonatesLabel=new ArrayList<>(List.of(new Point(3,120),new Point(72,106),new Point(159,105),
            new Point(352,109),new Point(441,109),new Point(541,118),new Point(550,151),new Point(522,244),
            new Point(518,540),new Point(360,560),new Point(145,560),new Point(7,550)));
    ShuffleGroup personnes=new ShuffleGroup();
    public Panel() throws IOException {

        this.setSize(616,678);
        this.setLocationRelativeTo(null);
        this.setUndecorated(true);

        Insets insets = panel.getInsets();

        panel.setOpaque(false);
        panel.setLayout(new BorderLayout());
        panelBg.addMouseListener(new MouseListener());
        panelBg.setLayout(new BorderLayout());
        close.setPreferredSize(new Dimension(90, 30));
        close.addActionListener(new BoutonListener());
        close.setBackground(Color.darkGray);
        close.setForeground(Color.GREEN);
        panelButton.setOpaque(false);
        panelButton.add(close);

        for (int i=0;i<12;i++){
            JLabel labelName=new JLabel(personnes.getPersonne().get(i));
            labelName.setHorizontalAlignment(JLabel.CENTER);
            labelName.setVerticalAlignment(JLabel.CENTER);
            labelName.setBackground(new Color(79,75,63));
            labelName.setForeground(Color.WHITE);
            labelName.setOpaque(true);
            Dimension labelNamesize = new Dimension(65,15);
            labelName.setBounds(coordonatesLabel.get(i).x + insets.left, coordonatesLabel.get(i).y + insets.top,
                    labelNamesize.width, labelNamesize.height);
            panelBg.add(labelName);
        }

        labelBenoit.setHorizontalAlignment(JLabel.CENTER);
        labelBenoit.setVerticalAlignment(JLabel.CENTER);

        labelBenoit.setBackground(new Color(79,75,63));
        labelBenoit.setForeground(Color.ORANGE);
        labelBenoit.setOpaque(true);
        Dimension labelNamesize = new Dimension(60,15);
        labelBenoit.setBounds(253 + insets.left, 75 + insets.top,
                labelNamesize.width, labelNamesize.height);

        panelBg.add(labelBenoit);
        panelBg.add(panelButton, BorderLayout.SOUTH);
        panelBg.add(panel, BorderLayout.CENTER);
        this.add(panelBg);

        this.setVisible(true);






    }

    class BoutonListener implements ActionListener {
        //Redéfinition de la méthode actionPerformed()
        public void actionPerformed(ActionEvent arg0) {
            System.exit(0);
        }
    }
class MouseListener implements java.awt.event.MouseListener{
    @Override
    public void mouseClicked(MouseEvent e) {
        // TODO Auto-generated method stub
        System.out.println("Click: x= "+e.getX()+" y = "+e.getY());

    }
    //lorsque le curseur entre dans notre Jpanel
    @Override
    public void mouseEntered(MouseEvent e) {
        // TODO Auto-generated method stu
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mousePressed(MouseEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // TODO Auto-generated method stub

    }


}}

